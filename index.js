const options = { /* ... */ };

const express = require('express');
const app = express()
const server = require('http').createServer(app);
const io = require('socket.io')(server, options);
const adapter = (require('socket.io-redis'))({ host: 'localhost', port: 6379 });

io.adapter(adapter);

io.on('connection', socket => {
  // Authenticate user from userId somehow
  console.log(`${socket.id} connected`);
  socket.on('disconnect', () => {
    console.log(`${socket.id} disconnected`);
  });
  socket.on('subscribe_topic', ({ topic }) => {
    console.log(`${socket.id} joined ${topic}.`);
    socket.join(topic);
  });
});

app.use(express.json());

app.post('/publish', function (req, res) {
  if (!req.body.topic || !req.body.payload){
    res.json({success: false});
    return;
  }
  const { topic } = req.body;
  const { action, payload } = req.body.payload;
  console.log(topic);
  console.log(action);
  console.log(payload);
  io.sockets.to(topic).emit(action, payload);
  res.json({success: true});
});

server.listen(8000, ()=> {
  console.log('Socket.io server running on 8000 port');
});
